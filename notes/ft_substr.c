/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:10:10 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/29 13:42:05 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a substring from the string ’s’.
** The substring begins at index ’start’ and is of maximum size ’len’.
** PARAMETERS
** #1. The string from which to create the substring.
** #2. The start index of the substring in the string ’s’.
** #3. The maximum length of the substring.
** RETURN VALUE
** The substring. NULL if the allocation fails.
** EXTERNAL FUNCKTIONS
** malloc
*/

/*
** Создает новую строку, копируя в нее 'len' символов из строки 's',
** начиная с индекса 'start'.

*/

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*res;
	size_t	i; //EXTRA
	size_t	sl;

	if (s == NULL)
		return (NULL); // Проверка на наличие строки из которой надо копировать
	
	sl = ft_strlen(s);
	if (start > sl) // Проверка на то, что начало ('start') новой строки не выходит за пределы исходной строки 's' 
		return (ft_strdup("")); // Возвращем пустую строку (с '\0')
		// ft_strdup - Создает новую строку, копию строки 's', выделив на нее место malloc-ом

	if (len > sl) // Проверяем что значение'len' не слишком большое, не выходит за конец строки 's'
		len = sl - start; // длина исходной строки минус индекс начала саб-строки

	res = (char*)malloc(sizeof(char) * (len + 1)); // выделяем память под строку, используя её длину + 1 для '\0' 
	if (res == NULL) // Если выделение памяти не сработало
		return (NULL);

	i = 0; //EXTRA
	while (i < start)	//EXTRA
		i++;			// EXTRA
	
	ft_strlcpy(res, &s[i], len + 1); // Копируем в 'res' 'len + 1' знаков из исходной строки 's', начиная с индекса 'i' (== 'start');
	// Можно заменить, убрав i ft_strlcpy(res, &s[start], len + 1);
	// ft_strlcpy - Копирует 'size - 1' знаков из 'src' в 'dst'; Гарантированно заканчивает строку '\0'
	return (res);
}
