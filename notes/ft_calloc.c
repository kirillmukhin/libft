/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:54:28 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/27 12:26:01 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function allocates memory for an array of 'nmemb' elements
** of 'size' bytes each and returns a pointer to the allocated memory.
** The memory is set to zero. If 'nmemb' or 'size' is 0,
** then ft_calloc() returns either NULL,
** or a unique pointer value that can later be successfully passed to free().
** If the multiplication of 'nmemb' and 'size' would result
** in integer overflow, then calloc() returns an error. By contrast,
** an integer overflow would not be detected in the following call
** to ft_malloc(), with the result that an incorrectly sized
** block of memory would be allocated:
** malloc(nmemb * size);
** RETUN VALUE
** The ft_calloc() function returns a pointer to the allocated memory,
** which is suitably aligned for any built-in type.
** On error, function will return NULL. NULL may also be returned by
** a successful call to ft_calloc() with 'nmemb' or 'size' equal to zero.
** EXTERNAL FUNCTIONS
** malloc
*/

/*
** FT_CALLOC выделяет память для массива из 'nmemb' элементов размером 'size' и возвращает укзатель
** Выделенная память заполняется нулями
*/

void	*ft_calloc(size_t nmemb, size_t size)
{
	void *p;

	p = malloc(size * nmemb);
	if (p == NULL) // Если выделение не сработало - возвращаем NULL
		return (NULL);
	ft_bzero(p, size * nmemb); // Заполнить память нулями
	return (p);
}
