/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:54:26 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 23:37:03 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function returns a pointer to a new string
** which is a duplicate of the string 's'.
** Memory for the new string is obtained with malloc(3),
** and can be freed with free(3).
** RETURN VALUE
** On success, the ft_strdup() function returns a pointer
** to the duplicated string. It returns NULL if insufficient memory
** was available, with errno set to indicate the cause of the error.
** EXTERNAL FUNCTIONS
** malloc
*/

/*
** Создает новую строку, копию строки 's', выделив на нее место malloc-ом.
*/

char	*ft_strdup(const char *s)
{
	char *new;

	new = (char*)malloc(sizeof(char) * (ft_strlen(s) + 1)); // резервируем место под копию
	if (new == NULL)
		return (NULL); // возвращаем NULL если облом с резервацией
	ft_strlcpy(new, s, ft_strlen(s) + 1); //копируем строку 's' в созданный массив
	return (new);
}
