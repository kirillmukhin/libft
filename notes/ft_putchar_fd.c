/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 18:17:31 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 01:21:42 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Outputs the character ’c’ to the given file descriptor.
** PARAMETERS
** #1. The character to output.
** #2. The file descriptor on which to write.
** EXTERNAL FUNCTIONS
** write
*/

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}
