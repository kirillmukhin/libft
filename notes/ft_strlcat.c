/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:50:30 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/27 13:31:08 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function appends the NUL-terminated string 'src' to the end of 'dst'.
** It will append at most size - ft_strlen(dst) - 1 bytes,
** NUL-terminating the result.
** RETURN VALUES
** The ft_strlcat() function returns the total length of the string
** it tried to create. That means the initial length of 'dst' plus
** the length of 'src'. While this may seem somewhat confusing,
** it was done to make truncation detection simple.
**
** Note, however, that if ft_strlcat() traverses size characters
** without finding a NUL, the length of the string is considered to be size
** and the destination string will not be NUL-terminated (since there was no
** space for the NUL). This keeps ft_strlcat() from running off the end of a
** string. In practice this should not happen (as it means that either size
** is incorrect or that dst is not a proper “C” string).
** The check exists to prevent potential security problems in incorrect code.
*/

/*
** FT_STRLCAT присоединяет строку 'src' к концу 'dst'
** Гарантированно проставив '\0' в конце строки.
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dst_len;
	char	*p;
	int		append_space; // размер места, доступного для копирования

	dst_len = ft_strlen(dst); //Длина исходной строки
	p = &dst[dst_len]; // укахатель на конец строки ('\0') в 'dst' - откуда мы начнем присоединят 'src'
	append_space = (size - dst_len); // Считаем сколько места у нас есть для копирования 'src'
	if (append_space > 0)
		ft_strlcpy(p, src, append_space); //Если место есть - копируем 'src' в 'dst', начиная с конца строки 'dst'
	if (dst_len >= size) // Нам надо вернуть длину строки, которую мы пытались создать, но
		return (ft_strlen(src) + size); // если длинна исходной строки (до '\0') была больше чем 'size', значит размер строки,
										// что мы пытались создать, был бы равен длине 'src' + 'size'
						//TL:DR макс размер вставки: ('size' - 'dst_len' - 1) если 'dst_len' > 'size' нам некуда добавить 'src'
						// и мы прикидываем какой у него был бы размер.

	return (dst_len + ft_strlen(src)); // Возвращает сумму исходных длин 'dst' и 'src'
										// Т.е. Если 'src' не уместился полностью, то возвращенное значение будет больше чем длина 'dst' после присоединения
}
