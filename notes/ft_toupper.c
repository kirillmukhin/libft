/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:53:35 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 01:45:51 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function converts lowercase letters to uppercase
** If 'c' is an lowercase letter, ft_toupper() returns its uppercase
** equivalent, if a uppercase representation exists in the current locale.
** Otherwise, it returns 'c'.
** If 'c' is neither an unsigned char value nor EOF,
** the behavior of these functions is undefined.
** RETURN VALUE
** The value returned is that of the converted letter,
** or 'c' if the conversion was not possible.
*/

int	ft_toupper(int c)
{
	if (c >= 'a' && c <= 'z')
		return (c - 32);
	return (c);
}
