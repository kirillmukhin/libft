/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/05 15:11:57 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/27 12:44:07 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

/*
** Function fills the first 'n' bytes of the memory area
** pointed to by 's' with the constant byte 'c'.
** RETURN VALUE
** The ft_memset() function returns a pointer to the memory area 's'.
*/

/*
** Заполняет участрок памяти, размером 'n', сплошным байтом 'c'
** 
*/

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*x;
	unsigned char	chr;
	int				i;

	x = s;
	chr = (unsigned char)c;
	i = 0;
	while (n > 0)
	{
		x[i] = chr;
		i++;
		n--;
	}
	return (s);
}

// int main()
// {
// 	int i = 0;
// 	int arr[8] = {1, 2, 3, 4, 5, 6, 7, 8};
// 	while (i < 7)
// 	{
// 		printf("%i\n", arr[i]);
// 		i++;
// 	}
// 	printf("\n--\n--\n");
// 	ft_memset(arr, 0, sizeof(int) * 8);
// 	i = 0;
// 	while (i < 7)
// 	{
// 		printf("%i\n", arr[i]);
// 		i++;
// 	}
// 	return (0);
// }