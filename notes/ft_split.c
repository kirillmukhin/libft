/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:11:09 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/29 14:03:42 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns an array of strings
** obtained by splitting ’s’ using the character ’c’ as a delimiter.
** The array must be ended by a NULL pointer.
** PARAMETERS
** #1. The string to be split.
** #2. The delimiter character.
** RETURN VALUE
** The array of new strings resulting from the split.
** NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc, free
*/

/* TEST ("asdccccasd", c)
** Should return
** asd
** asd
*/


/*
** FT_SPLIT - Разбить одну строку на несколько, используя параметер 'c' как разделитель
** И поместить их в 2D массив, заканчивающийся NULL-указателем.
*/

/*
** Clearing array and freeing its memory.
** Subfunction called when memory allocation fails at some point.
**
** Освобождаем массив. Функция вызывается если выделение памяти в какой-то момент не срабатывает.
** Вызывается из sf_add_str
*/

static	char	**sf_free_arr(char **arr)
{
	size_t i;

	i = 0;
	while (arr[i])
	{
		if (arr[i] != NULL) // в цикле освобождается память, использовавшаяся для строк
							// в двухмерном массиве
			free(arr[i]);
		i++;
	}
	free(arr);
	return (NULL);
}

/*
** Count the number of strings that will come from splitting 's'
** by its delimeter 'c'.
**
** Считаем сколько строк (массивов) получится после разбития оригинальной строки.
*/

static	size_t	sf_row_count(char const *s, char c)
{
	size_t	rows;
	int		i;

	rows = 1;
	i = 0;
	while (s[i] != '\0') //идем до конца строки
	{
		if (s[i] == c) // если встречаем разделитель 'c' - прибавляем одну строку к счетчику (rows++)
		{
			while (s[i] == c) // если идут несколько разделителей подряд - пропускаем их
				i++;
			rows++;
		}
		i++;
	}
	return (rows);
}

/*
** Create, fill and return string with character
**
** Создаем новую строку, которую запихнем в 2D массив.
*/

static	char	*sf_fill_str(char const *s, char c)
{
	char		*string;
	size_t		i;

	i = 0;
	while (s[i] != c && s[i] != '\0') // считаем длину новой строки до следующего разделителя 'c' или до конца '\0'
		i++;
	string = ft_substr(s, 0, i); // копирует 'i' символов начиная с начала строки 's' в строку 'string'
	// память для строк массива malloc-ом выделяется в ft_substr
	// ft_substr создает новую строку; Копирует 'len'(i) символов из строки 's' (s), начиная с индекса 'start' (0).
	return (string);
}

/*
** Add strings to the created array of arrays.
** If memory allocation for any string fails — free the entire array.
**
** Заполняем 2D массив строками, которые создаются в sf_fill_str
** Если выделение памяти под строку не сработало - чистим память через sf_free_arr
*/

static char		**sf_add_str(char **arr, char const *s, char c)
{
	int	i;
	int	string;

	i = 0;
	string = 0; // Номер добавляемой строки (массива)
	while (s[i] != '\0') // проходим через каждое значение в строке до её конца
	{
		if (s[i] != c) // Если встретили знак который не является разделителем
		{
			arr[string] = sf_fill_str(&s[i], c); // Создаем новую строку начиная с этого знака и до первого разделителя 'c' (или '\0')
			while (s[i] != c && s[i] != '\0') // пролистываем индекс до следующего разделтеля или конца строки
				i++;
			if (arr[string] == NULL) // если выделение памяти обломалось
			{
				sf_free_arr(arr);
				return (NULL);
			}
			string++; // переключаемся на заполнение следующей строки (массива)
		}
		else
			i++; // идем дальше по строке, пока идут знаки разделителя
	}
	arr[string] = NULL; // Если ничего не нашли - кидаем NULL
	return (arr);
}

char			**ft_split(char const *s, char c)
{
	char	**arr;
	size_t	rows;

	if (s == NULL) // проверка на наличие строки
		return (0);
	rows = sf_row_count(s, c); // Считаем сколько строк (массивов) получится после разбития оригинальной строки.
	arr = (char**)malloc(sizeof(char*) * (rows + 1)); // выделяем память на данное количество строк + 1 для NULL
	if (arr == NULL) // Если выделение памяти не сработало - возвращаем NULL
		return (NULL);
	arr = sf_add_str(arr, s, c); //Заполняем созданный массив строками
	return (arr);
}
