/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 22:49:40 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/29 13:53:38 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function compares the first 'n' bytes (each interpreted as unsigned char)
** of the memory areas 's1' and 's2'.
** RETURN VALUE
** The ft_memcmp() function returns an integer less than,
** equal to, or greater than zero if the first 'n' bytes
** of 's1' is found, respectively, to be less than,
** to match, or be greater than the first 'n' bytes of 's2'.
** For a nonzero return value, the sign is determined
** by the sign of the difference between the first pair of
** bytes (interpreted as unsigned char) that differ in 's1' and 's2'.
** If 'n' is zero, the return value is zero.
*/

/*
** Сравнивает первые 'n' байт строк 's1' и 's2'
** Возвращет '0' если строки одинаковые;
** и разницу первых различных знаков, если строки не одинаковые
*/

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	long unsigned int	i;
	const unsigned char	*str1;
	const unsigned char	*str2;

	i = 0;
	str1 = (unsigned char*)s1;
	str2 = (unsigned char*)s2;
	while (n > 0 && i < n)
	{
		if (str1[i] != str2[i])
			return (str1[i] - str2[i]);
		i++;
	}
	return (0);
}
