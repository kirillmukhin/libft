/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 18:50:58 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/30 14:27:48 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
//#include <limits.h>
#include <math.h>
#include <bsd/string.h>

int		ft_atoi(const char *nptr) __attribute__((weak));
void	ft_bzero(void *s, size_t n) __attribute__((weak));
void 	*ft_calloc(size_t nmemb, size_t size) __attribute__((weak));
int 	ft_isalnum(int c) __attribute__((weak));
int		ft_isalpha(int c) __attribute__((weak));
int		ft_isascii(int c) __attribute__((weak));
int		ft_isdigit(int c) __attribute__((weak));
int		ft_isprint(int c) __attribute__((weak));
char 	*ft_itoa(int n) __attribute__((weak));
// void ft_lstadd_back(t_list **lst, t_list *new) __attribute__((weak));
// void ft_lstadd_front(t_list **lst, t_list *new) __attribute__((weak));
// void ft_lstclear(t_list **lst, void (*del)(void*)) __attribute__((weak));
// void ft_lstdelone(t_list *lst, void (*del)(void*))  __attribute__((weak));
// void ft_lstiter(t_list *lst, void (*f)(void *)) __attribute__((weak));
// t_list *ft_lstlast(t_list *lst) __attribute__((weak));
// t_list *ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *)) __attribute__((weak));
// t_list *ft_lstnew(void *content) __attribute__((weak));
// int ft_lstsize(t_list *lst) __attribute__((weak));
void	*ft_memccpy(void *dest, const void *src, int c, size_t n) __attribute__((weak));
void 	*ft_memchr(const void *s, int c, size_t n) __attribute__((weak));
int		ft_memcmp(const void *s1, const void *s2, size_t n) __attribute__((weak));
void	*ft_memcpy(void *dest, const void *src, size_t n) __attribute__((weak));
void	*ft_memmove(void *dest, const void *src, size_t n) __attribute__((weak));
void	*ft_memset(void *s, int c, size_t n) __attribute__((weak));
void	ft_putchar_fd(char c, int fd) __attribute__((weak));
void 	ft_putendl_fd(char *s, int fd) __attribute__((weak));
void	ft_putnbr_fd(int n, int fd) __attribute__((weak));
void	ft_putstr_fd(char *s, int fd) __attribute__((weak));
char	**ft_split(char const *s, char c) __attribute__((weak));
char 	*ft_strchr(const char *s, int c) __attribute__((weak));
char 	*ft_strdup(const char *s) __attribute__((weak));
char 	*ft_strjoin(char const *s1, char const *s2) __attribute__((weak));
size_t	ft_strlcat(char *dst, const char *src, size_t size) __attribute__((weak));
size_t	ft_strlcpy(char *dst, const char *src, size_t size) __attribute__((weak));
size_t	ft_strlen(const char *s) __attribute__((weak));
char 	*ft_strmapi(char const *s, char (*f)(unsigned int, char)) __attribute__((weak));
int		ft_strncmp(const char *s1, const char *s2, size_t n) __attribute__((weak));
char	*ft_strnstr(const char *big, const char *little, size_t len) __attribute__((weak));
char	*ft_strrchr(const char *s, int c) __attribute__((weak));
char 	*ft_strtrim(char const *s1, char const *set) __attribute__((weak));
char 	*ft_substr(char const *s, unsigned int start, size_t len) __attribute__((weak));
int		ft_tolower(int c) __attribute__((weak));
int		ft_toupper(int c) __attribute__((weak));


int verbose = 0;
int cell_width = 64;

void color_red () {
  printf("\033[1;31m");
}

void color_green () {
  printf("\033[1;32m");
}

void color_reset () {
  printf("\033[0m");
}

void diff() {
	color_red();
	printf(" [DIFF] ");
	color_reset();
}
void ok() {
	color_green();
	printf(" [OK] ");
	color_reset();
}

void search_msg(char *name, int part)
{
	int i_smsg;
	int name_len = strlen(name);
	//int box_len = name_len*2 - 2; // full length minus 2
	int box_len;
	if (name_len < cell_width)
		box_len = cell_width - 2;
	else
		box_len = name_len*2 - 2;
	int fill_1 = ((box_len-name_len)/2);
	//int fill_2 = ((box_len-(name_len + 10))/2);
	int evenodd = (name_len % 2);
	printf("┏");
	for (i_smsg = 0; i_smsg < box_len; i_smsg++)
		printf("━");
	printf("┓\n");
	printf("┃");

	for (i_smsg = 0; i_smsg < fill_1; i_smsg++)
		printf(" ");
	printf("%s", name);
	for (i_smsg = 0; i_smsg < fill_1; i_smsg++)
		printf(" ");

	if (evenodd != 0)
		printf(" ");
	printf("┃\n");
	printf("┗");
	for (i_smsg = 0; i_smsg < box_len; i_smsg++)
		printf("━");
	printf("┛\n");


	printf("╔");
	for(i_smsg = 0; i_smsg < (cell_width - 2); i_smsg++ )
		printf("═");
	printf("╗\n");


	printf(" - PART: ");
	if (part == 1)
		printf("1");
	else if (part == 2)
		printf("2");
	else if (part == 3)
		printf("3");
	else
		printf("???");
	printf(" -\n");
}

void separator()
{
	int i_sep;
	printf(" ");
	for (i_sep = 0; i_sep < 32; i_sep++)
		printf("┈");
	printf("\n");
}

void check(char *type, void *input, void *og, void *ft)
{
	/*
	** "command" - value passed to functions, value recieved from both functions
	** "ii" - int, int
	** "ic" - int, char
	** "ci" - char, int
	** "cc" -char, char
	*/
	int error = 0;

	if	(
		((type[1] == 'i') && (*(int*)og == *(int*)ft))
		||
		((type[1] == 'c') && ((char*)og == (char*)ft))
		)
		error = 0;
	else
		error = 1;

	if (error||verbose)
	{
		if (type[0] == 'i')
			printf(" Checking \"%d\"\n", *(int*)input);
		else
			printf(" Checking \"%s\"\n", (char*)input);
	}

	if	(error == 0)
	{
		if (verbose == 1)
		{
			color_green();
			printf(" [OK] ");
		}
	}
	else
	{
		color_red();
		printf(" [DIFF] ");
	}
	color_reset();

	if (verbose||error)
	{
		if (type[1] == 'i')
			printf("\n og: \"%d\",\n ft: \"%d\"\n", *(int*)og, *(int*)ft);
		else
			printf("\n og: \"%s\",\n ft: \"%s\"\n", (char*)og, (char*)ft);
		separator();
	}
}

void errors_count(int total, int errors)
{
	int passed = (total - errors);
	if (errors != 0)
		{
			printf(" Tests: %d/%d", passed, total);
			printf(" [Errors: ");
			color_red();
			printf("%d", errors);
			color_reset();
			printf("]\n");
		}
	else
	{
		printf(" Tests: %d/%d [", passed, total);
		color_green();
		printf("✓");
		color_reset();
		printf("]\n");
	}
}

void end_msg()
{
	int i_end_msg;
	printf("╚");
	for(i_end_msg = 0; i_end_msg < (cell_width - 2); i_end_msg++ )
		printf("═");
	printf("╝\n\n\n");
}

  //////////////////////////////////////
 //				TESTS				//
/////////////////////////////////////

void test_atoi()
{
	search_msg("ft_atoi.c", 1);
	int atoi_tests = 0;
	int atoi_errors = 0;

	int i_atoi = 0;
	char arr_atoi[14][64] = {
	"420",
	"-421",
	"0",
	"",
	"  422abc",
	"abc423",
	"++424",
	"--424",
	"♠♠♠123",
	//UNDEFINED BEHAVIOR
	"-9223372036854775828",
	"-9223372036854775028",
	"-9223372036854775812_LONG_MIN-4_undefined_behaviour", //LONG_MIN - 4
	"+9223372036854775811_LONG_MAX+4_undefined_behaviour", //LONG_MAX + 4
	"18446744073709551619_ULONG_MAX+4_undefined_behaviour" //ULONG_MAX + 4
	};
	while(i_atoi < 14)
	{
		int res_atoi_og = atoi(arr_atoi[i_atoi]);
		int res_atoi_ft = ft_atoi(arr_atoi[i_atoi]);
		if (res_atoi_ft != res_atoi_og)
			atoi_errors += 1;
		check("ci", arr_atoi[i_atoi], &res_atoi_og, &res_atoi_ft);
		atoi_tests += 1;
		i_atoi++;
	}
	// char atoi_overflow[] = "-9223372036854775828";
	// printf(" atoi vs atol");
	// printf(" input: '%s'\n atol: %li\n atoi: %i\n ft_atoi: %i\n", atoi_overflow, atol(atoi_overflow), atoi(atoi_overflow), ft_atoi(atoi_overflow));
	// errors_count(atoi_tests, atoi_errors);
	end_msg();
}

void test_bzero()
{
	search_msg("ft_bzero", 1);
	end_msg();
}

void test_calloc()
{
	search_msg("ft_calloc", 1);
	int size = 32768;

 	void * d1 = ft_calloc(size, sizeof(int));
	void * d2 = calloc(size, sizeof(int));
	if (memcmp(d1, d2, size * sizeof(int)))
		printf(" Fail\n");
	free(d1);
	free(d2);
	printf(" Success\n");
	end_msg();
}

void test_isalnum()
{
	search_msg("ft_isalnum", 1);
	end_msg();
}

void test_isalpha()
{
	search_msg("ft_isalpha", 1);
	end_msg();
}

void test_isascii()
{
	search_msg("ft_isascii.c", 1);
	int isascii_tests = 0;
	int isascii_errors = 0;

	int i_ascii = 127;
	char arr_ascii[9][3] = {
	"€",
	"♠",
	"⊗",
	"Ω",
	"æ",
	"Ë",
	"©",
	"£",
	"¡"
	};
	while (i_ascii >= 0 && i_ascii < 128)
	{
		int res_ascii_orig = isascii(i_ascii);
		int res_ascii_ft = ft_isascii(i_ascii);
		if (res_ascii_ft != res_ascii_orig)
		{
			diff();
			printf(" isascii: %d, ft: %d\n",res_ascii_orig, res_ascii_ft);
			isascii_errors++;
		}
		isascii_tests++;
		i_ascii--;
	}
	while (i_ascii < 8)
	{
		int res_ascii_orig = isascii(i_ascii);
		int res_ascii_ft = ft_isascii(i_ascii);
		if (res_ascii_ft != res_ascii_orig)
			isascii_errors++;
		check("ci", arr_ascii[i_ascii], &res_ascii_orig, &res_ascii_ft);
		isascii_tests++;
		i_ascii++;
	}
	errors_count(isascii_tests, isascii_errors);
	end_msg();
}

void test_isdigit()
{
	search_msg("ft_isdigit.c", 1);
	int isdigit_tests = 0;
	int isdigit_errors = 0;
	int arr_isdigit[19] = {
	' ',
	'0',
	'1',
	'2',
	'3',
	'4',
	'5',
	'6',
	'7',
	'8',
	'9',
	'A',
	'a',
	'Z',
	'z',
	'-',
	'+'
	};

	int i_isdigit = 0;
	while (i_isdigit < 19)
	{
		//int arr_isdigit_val = arr_isdigit(i_isdigit); // a single value from testing array
		int res_isdigit_og = isdigit(arr_isdigit[i_isdigit]);
		int res_isdigit_ft = ft_isdigit(arr_isdigit[i_isdigit]);
		if (res_isdigit_ft != res_isdigit_og)
			isdigit_errors += 1;
		isdigit_tests += 1;
		check("ii", &arr_isdigit[i_isdigit], &res_isdigit_og, &res_isdigit_ft);
		i_isdigit++;
	}
	errors_count(isdigit_tests, isdigit_errors);
	end_msg();
}

void test_isprint()
{
	search_msg("ft_isprint.c", 1);
	end_msg();
}

void test_itoa()
{
	search_msg("ft_itoa.c", 2);
	int arr_itoa[8] = {0, 1, 2, 10, 420, -421,-20484096, 40968216};
	int i_itoa = 0;

	while (i_itoa < 8)
	{
		char *res_itoa_ft = ft_itoa(arr_itoa[i_itoa]);
		printf(" Checking: %d\n ft: \'%s\'\n", arr_itoa[i_itoa], res_itoa_ft);
		i_itoa++;
	}
	end_msg();
}

// void test_lstadd_back()
// void test_lstadd_front()
// void test_lstclear()
// void test_lstdelone()
// void test_lstiter()
// void test_lstlast()
// void test_lstmap()
// void test_lstnew()
// void test_lstsize()
void test_memccpy()
{
	search_msg("ft_memccpy.c", 1);

	end_msg();
}
// void test_memchr()

void test_memcmp()
{
	search_msg("ft_memcmp.c", 1);
	int memcmp_tests = 0;
	int memcmp_errors = 0;

	int i_memcmp = 0;
	char arr_memcmp_1[24][32] = {
		"01234QWERTYUIOPASDFGHJKLZXCVBNM",
		"5 67890-=;}{[]<.>/`\\|/?\"'@!#$",
		"Did you ever hear the tragedy",
		" of Darth Plagueis The Wise?",
		" I thought not. It’s not a",
		" story the Jedi would tell you.",
		" It’s a Sith legend. Darth",
		" Plagueis was a Dark Lord of",
		" the Sith, so powerful and so",
		" wise he could use the Force",
		" to influence the midichlorians",
		" to create life… He had such",
		" a knowledge of the dark side",
		" that he could even keep the",
		" ones he cared about from",
		" dying. The dark side of the",
		" Force is a pathway to many",
		" abilities some consider",
		" to be unnatural.",
		"01234QWERTYUIOPASDFGHJKLZXCVBNM",
		"5 67890-=;}{[]<.>/`\\|/?\"'@!#$",
		"AAAAAAAAAAAAAAAAAAAAAAAAAAA",
		"BBBBBBBBBBBBBBBBBBBBBBBBBBB",
		"CCCCCCCCCCCCCCCCC",
	};
	char arr_memcmp_2[24][32] = {
		"01234QWERTYUIOPASDFGHJKLZXCVBNM",
		"5 67890-=;}{[]<.>/`\\|/?\"'@!#$",
		"Did you ever hear the tragedy",
		" of Darn Pootis The Wide?",
		" I thought so. It’s not a",
		" story the Judi would tell you.",
		" It’s a Sims legend. Darn",
		" Pootis was a Dark Lord of",
		" the Sith, so powerful and so",
		" wise he could use the Farce",
		" to influence the midichlorians",
		" to create wifi… He had such",
		" a knowledge of the dark side",
		" that he could even keep the",
		" ones he cared about from",
		" dying. The dark side of the",
		" Farce is a pathway to many",
		" abilities some consider",
		" to be unnnatural.",
		"01234QWERTYUIOPASDFGHJKLZXXXXXX",
		"5 67890-=;}{[]<.>/`\\|/?\"XXXXX",
		"AAOAAAAAAAAAAAAAAAAAAAAAAAA",
		"BBBBBBBBBBBBBBBBB",
		"CCCCCCCCCCCCCCCCCCCCCCCCCCC",
	};
	int len_memcpm = sizeof(arr_memcmp_1)/sizeof(arr_memcmp_1[0]);
	while(i_memcmp < len_memcpm)
		{
			int res_memcmp_orig = memcmp(arr_memcmp_1[i_memcmp], arr_memcmp_2[i_memcmp], 25);
			int res_memcmp_ft = ft_memcmp(arr_memcmp_1[i_memcmp], arr_memcmp_2[i_memcmp], 25);
			if (res_memcmp_ft != res_memcmp_orig)
				{
					printf("Comparing \"%s\" and \"%s\"\n",arr_memcmp_1[i_memcmp], arr_memcmp_2[i_memcmp]);
					diff();
					printf(" memcmp: %d, ft: %d\n", res_memcmp_orig, res_memcmp_ft);
					memcmp_errors += 1;
					separator();
				}
			else
			{
				if (verbose != 0)
					{
						printf("Comparing \"%s\" and \"%s\"\n",arr_memcmp_1[i_memcmp], arr_memcmp_2[i_memcmp]);
						ok();
						printf(" memcmp: %d, ft: %d\n", res_memcmp_orig, res_memcmp_ft);
						separator();
					}
			}
			memcmp_tests += 1;
			i_memcmp += 1;
		}
	errors_count(memcmp_tests, memcmp_errors);
	end_msg();
}

void test_memcpy()
{
	search_msg("ft_memcpy.c", 1);
	int memcpy_tests = 1;
	int memcpy_errors = 0;

	int i_memcpy = 0;
	char s02[] = "ABCXYZ0123456789:/\".?!@#$()";
	int len_memcpy = sizeof(s02)/sizeof(s02[0]);
	char s00[len_memcpy];
	char s01[len_memcpy];
	memcpy(s00, s02, len_memcpy);
	ft_memcpy(s01, s02, len_memcpy);
	while (i_memcpy < len_memcpy)
	{
		if (s00[i_memcpy] != s01[i_memcpy])
		{
			diff();
			printf(" input: %s\n", s02);
			printf("memcpy: %s, ft: %s\n", s00, s01);
			memcpy_errors += 1;
			separator();
		}
		i_memcpy += 1;
	}
	if (verbose != 0 && memcpy_errors == 0)
	{
		ok();
		printf(" input: %s\n", s02);
		printf("memcpy: %s, ft: %s\n", s00, s01);
		separator();
	}
	errors_count(memcpy_tests, memcpy_errors);
	end_msg();
}

// test_memmove()

void test_memset()
{
	search_msg("ft_memset.c", 1);
	int memset_tests = 2;
	int memset_errors = 0;

	int i_memset;
	char arr_memset_0[64];
	for(i_memset = 0; i_memset < 64; i_memset++) {
		arr_memset_0[i_memset] = 'r';
	}

	if (memset(arr_memset_0, 'A', 8) != ft_memset(arr_memset_0, 'A', 8))
	{
		diff();
		printf(" memset != ft_memset when comaring array of CHAR\n");
		memset_errors += 1;
	}
	else
	{
		if (verbose != 0)
		{
			ok();
			printf(" memset = ft_memset when comparing array of CHAR\n");
		}
	}
	separator();
	int arr_memset_1[64];
	for (i_memset = 0; i_memset < 64; i_memset++) {
		arr_memset_1[i_memset] = i_memset * 64;
	}
	if (memset(arr_memset_1, 1, 8) != ft_memset(arr_memset_1, 1, 8))
	{
		diff();
		printf(" memset != ft_memset when comaring array of INT\n");
		memset_errors += 1;
	}
	else
	{
		if (verbose != 0)
		{
			ok();
			printf(" memset = ft_memset when comparing array of INT\n");
		}
	}
	errors_count(memset_tests, memset_errors);
	end_msg();
}

// void test_putchar_fd()
// void test_putendl_fd()

void test_putnbr()
{
	search_msg("ft_putnbr_fd.c", 2);
	int putnbr_tests = 0;
	int putnbr_errors = 0;
	int i_putnbr = 0;
	int arr_putnbr[8] = {0, 42, -32767, 65535, -2147483647, 2147483647, -2147483648, 2147483648};
	while(i_putnbr < 8)
		{
			ft_putchar_fd('\n', 1);
			ft_putnbr_fd(arr_putnbr[i_putnbr], 1);
			ft_putchar_fd('\n', 2);
			ft_putnbr_fd(arr_putnbr[i_putnbr], 2);
			i_putnbr++;
			putnbr_tests += 1;
		}
	printf("\n");
	errors_count(putnbr_tests, putnbr_errors);
	end_msg();
}

// void test_putstr_fd()

void test_split()
{
	search_msg("ft_split.c", 2);
	// char *split_test0 = "111s222ss333sss4444";
	// char delimeter0 = 's';
	char *split_test1 = "      split       this for   me  !       ";
	char delimeter1 = ' ';
  // char *split_test2 = "";
	// char delimeter2 = 0;
	char **arr_split = ft_split(split_test1, delimeter1);
	int i = 0;
	//int arr_rows = sizeof(arr_split)/sizeof(arr_split[0]);
	printf(" input: \"%s\"\n", split_test1);
	printf(" delimeter: \"%c\"\n", delimeter1);
	while (i < 8)
	{
		printf(" ft: \"%s\"\n", arr_split[i]);
		i++;
	}

	end_msg();
}
// void test_strchr()
// void test_strdup()

void test_strjoin()
{
	search_msg("ft_strjoin.c", 2);

	char strjoin1[] = "string1";
	char strjoin2[] = "string2";

	printf(" input: %s, %s\n output: %s\n", strjoin1, strjoin2, ft_strjoin(strjoin1,strjoin2));

	end_msg();
}

void test_strlcat()
{
	search_msg("ft_strlcat.c", 1);

		char *dst;
		size_t i_test_strlcat = 15;
		dst = (char *)malloc(sizeof(*dst) * i_test_strlcat);
		memset(dst, 'r', 6);
		char *src = "lorem ipsum dolor sit amet";

		size_t i_strlcat;

		printf(" dst: %s, src: %s\n", dst, src);

		i_strlcat = ft_strlcat(dst, src, i_test_strlcat);
		printf(" dst after: %s\n return of ft_strlcat: %ld\n", dst, i_strlcat);

		end_msg();
}

// void test_strlcpy()
// {

// }

void test_strlen()
{
	search_msg("ft_strlen.c",1);
	char strlen_test0[] = "Ni!";
	char strlen_test1[] = "Tis' but a scratch!";
	char strlen_test2[] = "He's not the messiah, he’s a very naughty boy!";
	char strlen_test3[] = 	"Egg and bacon \nEgg, sausage and bacon \nEgg and Spam \
    						\nEgg, bacon and Spam \nEgg, bacon, sausage and Spam\n \
    						\nSpam, bacon, sausage and Spam \
							\nSpam, egg, Spam, Spam, bacon and Spam \
							\nSpam, Spam, Spam, egg and Spam \
							\nSpam, Spam, Spam, Spam, Spam, Spam, baked beans, Spam, Spam, Spam and Spam \
							\nLobster Thermidor aux crevettes with a Mornay sauce, garnished with truffle pâté, brandy and a fried egg on top, and Spam.";

	//check(char *type, char *res, void *a, void *b, void *c)
	if ((ft_strlen(strlen_test0) == strlen(strlen_test1))&&(ft_strlen(strlen_test1) == strlen(strlen_test2))&&(ft_strlen(strlen_test2) == strlen(strlen_test3))&&(ft_strlen(strlen_test3) == strlen(strlen_test0)))
		printf("Ok\n");
	else
		printf("fart\n");
	end_msg();
}

void test_strmapi()
{
	search_msg("ft_strmapi.c", 2);
	char string0[16] = "ABCDEFGHIJKLMNO";
	//check("ci", "ok", arr_atoi[i_atoi], &res_atoi_orig, &res_atoi_ft);
	printf("test %s\n", string0);
	end_msg();
}

void test_strncmp()
{
	search_msg("ft_strncmp.c", 1);
	long unsigned int i_strncmp = 0;
	int i_strncmp1 = 0;

	int arr_strncmp_n [11] = {
		5,
		5,
		4,
		12,
		8,
		3,
		4,
		0,
		0,
		6,
		6
	};
	char arr_strncmp [22][16] = {
	// 5
	"",
	"empty",
	// 5
	"empty2",
	"",
	// 4
	"same",
	"same",
	// 12
	"almost",
	"almostsame",
	// 8
	"register",
	"rEgister",
	// 3
	"abcdefghij",
	"abcdefgxyz",
	// 4
	"abcdefgh",
	"abcdwxyz",
	// 0
	"zyxbcdefgh",
	"abcdwxyz",
	// 0
	"empty_0_check",
	"",
	// 6
	"spam\200",
	"spam\0",
	// 6
	"spam\0",
	"spam\300"
	};

	while (i_strncmp < 22) // while index is lower then number of items in the test arrays.
	{
		//allocate memory for the description string (summ of lenght of two strings we are comparing) and
		char *type_strncmp = malloc( sizeof(char) * ( strlen(arr_strncmp[i_strncmp]) + strlen(arr_strncmp[i_strncmp + 1]) + 64) );
		type_strncmp = strcat(&*type_strncmp, "'");
		type_strncmp = strcat(&*type_strncmp, &*arr_strncmp[i_strncmp]);
		type_strncmp = strcat(&*type_strncmp, "' and '");
		type_strncmp = strcat(&*type_strncmp, &*arr_strncmp[i_strncmp + 1]);
		type_strncmp = strcat(&*type_strncmp, "' for ");

		// Getting the LENGTH of the number that we pass to the function as the number of bytes to compare
		int num_len = 0;
		int n = arr_strncmp_n[i_strncmp1];
		while (n != 0)
		{
			n = n/10;
			num_len++;
		}

		// allocate memory for the string with number, based on its previously found lenght and type
		char *n_strncmp = malloc( sizeof(char) * num_len);
		// turn int into char, using allocate space
		printf(n_strncmp, "%d", arr_strncmp_n[i_strncmp1]);
		type_strncmp = strcat(&*type_strncmp, &*n_strncmp);
		type_strncmp = strcat(&*type_strncmp, " bytes");

		// use function that we're testing
		int res_strncmp_og = strncmp(arr_strncmp[i_strncmp], arr_strncmp[i_strncmp + 1], arr_strncmp_n[i_strncmp1]);
		int res_strncmp_ft = ft_strncmp(arr_strncmp[i_strncmp], arr_strncmp[i_strncmp + 1], arr_strncmp_n[i_strncmp1]);
		//compare results
		check("ci", &*type_strncmp, &res_strncmp_og, &res_strncmp_ft);
		//free memory and move counters
		free(n_strncmp);
		free(type_strncmp);
		i_strncmp = i_strncmp + 2;
		i_strncmp1++;
	}
	end_msg();
}

// void test_strnstr()
// void test_strrchr()

void test_strtrim()
{
	search_msg("ft_strtim.c", 2);
	printf("\n input: \"qweASDqwe,ewq\"\n output: %s\n",ft_strtrim("qweASDqwe", "ewq"));
	printf("\n input: \"         \"\n output: %s\n",ft_strtrim("         ", "\t \n"));

	end_msg();
}

void test_substr()
{
	search_msg("ft_substr.c", 2);

	printf("\n input: (\"\\0\",0,3)\n output should be empty: ''\n"
						" '%s'\n",
						ft_substr("\0",0,1));
	printf("\n input: (\"\",0,3)\n"
						" output should be empty: ''\n '%s'\n",
						ft_substr("",0,3));
	printf("\n input: (\"test\",1,6)\n"
						" output should be: 'est'\n '%s'\n",
						ft_substr("test",1,6));
	printf("\n input: (\"onetwo\\0threefour\\0five\",4,14)\n"
						" output should be: 'wo'\n '%s'\n",
						ft_substr("onetwo\0threefour\0five",4,14));
	printf("\n input: (\"nnnnyyyyn\",4,4)\n"
						" output should be: 'yyyy'\n '%s'\n",
						ft_substr("nnnnyyyyn",4,4));
	printf("\n input: ((\"alla\",5,0)\n"
						" len > start, output should be empty: ''\n '%s'\n",
						ft_substr("alla",5,0));
	printf("\n input: ((\"start>strlen\",16,16)\n"
						" output should be empty: ''\n '%s'\n",
						ft_substr("start>strlen",16,16));

	end_msg();
}

void test_tolower()
{
	search_msg("ft_tolower.c", 1);

	printf(" input: A, output: %c\n", tolower('A'));
	printf(" input: Z, output: %c\n", tolower('Z'));
	printf(" input: b, output: %c\n", tolower('b'));

	end_msg();
}

void test_toupper()
{
	search_msg("ft_toupper.c", 1);

	printf(" input: x, output: %c\n", toupper('x'));
	printf(" input: y, output: %c\n", toupper('y'));
	printf(" input: Z, output: %c\n", toupper('Z'));

	end_msg();
}


// void test_func()
// {
// 	search_msg("ft_func.c", 1);
// 	int func_tests = 0;
// 	int func_errors = 0;
// 	int i_func = 0;
// 	for()
// 		{
// 			if ()
// 				{
// 					diff();
// 					printf("");
// 					func_errors += 1;
// 					separator();
// 				}
// 			else if (verbose != 0)
// 				{
// 					printf("");
// 					ok();
// 					printf("");
// 					separator();
// 				}
// 			func_tests += 1;
// 		}
// 	errors_count(func_tests, func_errors);
// 	end_msg();
// }

int	main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;
	int i_args = 0;
	if (argc > 0)
	{
		while (i_args < argc)
		{
			if (strncmp(argv[i_args], "verbose", 8) == 0)
				verbose = 1;
			i_args++;
		}
	}
	if (verbose == 0)
	{
		printf("Verbose mode is OFF ");
		printf("[use argument verbose to enable]\n");
	}
	else
		printf("Verbose mode is ON\n");

  /////////////////////////////
 // call functions below	//
/////////////////////////////

	if (ft_atoi) test_atoi();
	if (ft_bzero) test_bzero(); //
	if (ft_calloc) test_calloc(); //
	if (ft_isalnum) test_isalnum(); //
	if (ft_isalnum) test_isalpha(); //
	if (ft_isascii) test_isascii();
	if (ft_isdigit) test_isdigit();
	if (ft_isprint) test_isprint();
	if (ft_itoa) test_itoa();
	// if (ft_lstadd_back) test_lstadd_back();
	// if (ft_lstadd_front) test_lstadd_front();
	// if (ft_lstclear) test_lstclear();
	// if (ft_lstdelone) test_lstdelone();
	// if (ft_lstiter) test_lstiter();
	// if (ft_lstlast) test_lstlast();
	// if (ft_lstmap) test_lstmap();
	// if (ft_lstnew) test_lstnew();
	// if (ft_lstsize) test_lstsize();
	// if (ft_memccpy) test_memccpy();
	// if (ft_memchr) test_memchr();
	if (ft_memcmp) test_memcmp();
	if (ft_memcpy) test_memcpy();
	// if (ft_memmove) test_memmove();
	if (ft_memset) test_memset();
	// if (ft_putchar_fd) test_putchar_fd();
	// if (ft_putendl_fd) test_putendl_fd();
	// if (ft_putnbr_fd) test_putnbr_fd();
	// if (ft_putstr_fd) test_putstr_fd();
	if (ft_split) test_split();
	// if (ft_strchr) test_strchr();
	// if (ft_strdup) test_strdup();
	if (ft_strjoin) test_strjoin();
	if (ft_strlcat) test_strlcat();
	// if (ft_strlcpy) test_strlcpy();
	if (ft_strlen) test_strlen();
	if (ft_strmapi) test_strmapi();
	if (ft_strncmp) test_strncmp();
	// if (ft_strnstr) test_strnstr();
	// if (ft_strrchr) test_strrchr();
	if (ft_strtrim) test_strtrim();
	if (ft_substr) test_substr();
	if (ft_tolower) test_tolower();
	if (ft_toupper) test_toupper();


  /////////////////////
 //		 EXIT		//
/////////////////////
	return (0);
}
