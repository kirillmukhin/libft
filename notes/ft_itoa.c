/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:11:28 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/29 14:16:34 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a string
** representing the integer received as an argument.
** Negative numbers must be handled.
** PARAMETERS
** #1. the integer to convert.
** RETURN VALUE
** The string representing the integer. NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc
*/

/*
** Число -> строка
** Переводит целое число 'n' в строку, резервируя на нее память.
*/

static	int	sf_int_len(int n)
{
	int len;

	len = 0;
	if (n <= 0)
		len++;
	while (n != 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

/*
** Check for zero and negative values;
** Includes check for the INT_MIN
*/

static	int	sf_spec(char *str, int n) // Проверка для случаев где 'n' <= 0
{
	if (n == 0)
		str[0] = '0'; //Возвращаем или '0'
	else
		str[0] = '-'; //Или '-'
	if (n == INT_MIN) // Проверяем исключение
	{
		str[1] = '2';
		return (147483648);
	}
	return (-n);
}

char		*ft_itoa(int n)
{
	char	*str;
	int		len;

	len = sf_int_len(n);
	str = (char*)malloc(sizeof(char) * (len + 1));
	if (str == NULL)
		return (NULL);
	if (str)
	{
		if (n <= 0)
			n = sf_spec(str, n);
		str[len] = '\0';
		len--;
		while (n > 0)
		{
			str[len] = n % 10 + '0';
			n = n / 10;
			len--;
		}
	}
	return (str);
}
