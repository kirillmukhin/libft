/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:50:41 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/28 11:50:48 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function is copying strings. It's designed to be safer, more consistent,
** and less error prone replacements for strncpy(3).
** Unlike that function, ft_strlcpy() takes the full size of the buffer
** (not just the length) and guarantee to NUL-terminate the result
** (as long as size is larger than 0).  Note that a byte for the NUL
** should be included in 'size'. Also note that ft_strlcpy() only operate
** on true “C” strings. This means that src must be NUL-terminated.
**
** The ft_strlcpy() function copies up to 'size' - 1 characters from the
** NUL-terminated string 'src' to 'dst', NUL-terminating the result.
**
** RETURN VALUES
** The ft_strlcpy() function returns the total length of the string it
** tried to create. That means the length of 'src'.
** While this may seem somewhat confusing, it was done to make
** truncation detection simple.
*/

/*
** Копирует 'size - 1' знаков из 'src' в 'dst'
** Гарантированно заканчивает строку '\0'
*/

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	char	*p;

	if (dst == NULL && src == NULL)
		return (0);
	p = ft_memccpy(dst, src, '\0', size);
	if (p == NULL && size != 0)
		dst[size - 1] = '\0';
	return (ft_strlen(src));
}
