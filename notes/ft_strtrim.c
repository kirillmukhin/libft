/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:10:48 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 22:41:30 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a copy of ’s1’
** with the characters specified in ’set’
** removed from the beginning and the end of the string.
** PARAMETERS
** #1. The string to be trimmed.
** #2. The reference set of characters to trim.
** RETURN VALUE
** The trimmed string. NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc
*/

/*
** FT_STRTRIM Возвращает строку 's1' с отрезанными с краев символами из параметра 'set'
** Возвращаем все в новом массиве
*/

#include <stdio.h>

char	*ft_strtrim(char const *s1, char const *set) // s1 - строка, set - символы, которые надо срезать
{
	size_t	len;
	int		i;

	i = 0;
	if (s1 == NULL) // Если строка пустая - возвращаем NULL
		return (NULL);
	if (set == NULL) // Если символов нет - возвращаем строку s1
		return (ft_strdup(s1));
	while (s1[i] != '\0' && ft_strchr(set, s1[i]) != NULL) //Пока не дошли до конца строки и текущий символ s1[i] есть в set => подлежит срезке.
		i++; //Идем по строке, пока не появится первый символ не из 'set' (первый символ, что мы оставим) или строка не закончится.
	len = ft_strlen(&s1[i]); //Измеряем длинну строки от текущей позиции.
	while (len != 0 && ft_strchr(set, s1[i + len])) 
		len--;
	return (ft_substr(&s1[i], 0, len + 1));
}
// FT_STRCHR возвращает указатель на первое появление текущего знака s1[i] в списке символов set. NULL - символ не найден.
