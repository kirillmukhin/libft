/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/05 15:12:13 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/27 12:27:55 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function erases the data in the 'n' bytes of the memory
** starting at the location pointed to by 's',
** by writing zeros (bytes containing '\0') to that area.
*/

/*
** Стирает всё с начала строки 's' и до 'n', заполняя это место нулями '\0'
*/

void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}
