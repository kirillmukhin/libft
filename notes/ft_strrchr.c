/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:51:38 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/27 12:53:17 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function returns a pointer to the last occurrence
** of the character 'c' in the string 's'.
** Here "character" means "byte";
** these functions do not work with wide or multibyte characters.
** RETURN VALUE
** The ft_strrchr() function returns a pointer to the matched character
** or NULL if the character is not found. The terminating null byte
** is considered part of the string, so that if 'c' is specified as'\0',
** function returns a pointer to the terminator.
*/

/*
** FT_STRRCHR возвращает адрес последнего появления знака 'c' в строке 's'
**
** Обратная версия - ft_strrchr
*/

char	*ft_strrchr(const char *s, int c)
{
	int	i;

	i = ft_strlen(s);
	while (i >= 0)
	{
		if (s[i] == (char)c)
			return ((char*)&s[i]);
		i--;
	}
	return (NULL);
}
