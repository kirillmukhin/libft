/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:53:46 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 01:45:00 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function converts uppercase letters to lowercase
** If 'c' is an uppercase letter, ft_tolower() returns its lowercase
** equivalent, if a lowercase representation exists in the current locale.
** Otherwise, it returns 'c'.
** If 'c' is neither an unsigned char value nor EOF,
** the behavior of these functions is undefined.
** RETURN VALUE
** The value returned is that of the converted letter,
** or 'c' if the conversion was not possible.
*/

int	ft_tolower(int c)
{
	if (c >= 'A' && c <= 'Z')
		return (c + 32);
	return (c);
}
