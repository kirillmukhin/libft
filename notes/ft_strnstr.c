/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:51:53 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/29 14:11:38 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function locates the first occurrence of the null-terminated string
** little in the string big, where not more than 'len' characters are searched.
** Characters that appear after a ‘\0’ character are not searched.
** RETURN VALUE
** If little is an empty string, big is returned;
** if little occurs nowhere in big, NULL is returned;
** otherwise a pointer to the first character
** of the first occurrence of little is returned.
*/

/*
** Поиск строки 'little' внутри строки 'big', на протяжении 'len' знаков.
*/

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	i;
	size_t	g;

	i = 0;
	g = 0;
	if (ft_strlen(little) == 0) //Если строка little пустая
		return ((char*)big);
	while ((big[i] != '\0') && (i < len)) // Пока не дошли до конца проверяемого спектра 'len' или конца строки 'big' 
	{									  // Проходим по каждому знаку 'big[i]'
		if (little[g] == big[i]) // Если перый знак 'little[0]' совпадает с текушим big[i]
		{
			while ((big[i + g] == little[g]) && ((i + g) < len)) // Запускаем цикл и сверяем все знаки строк little и big с первого совпадения
			{
				if (little[g + 1] == '\0') // Если дошли до конца строки 'little' - строки полностью совпали
					return ((char*)&big[i]); //Возвращаем указатель на начало строки little
				g++;
			}
			g = 0; // Если строки совпали не полностью, обнуляем счетчик строки 'little' и, после, двигаем счетчик строки 'big'
		}
		i++;
	}
	return (NULL); //Если ни одного совпадения не было
}
