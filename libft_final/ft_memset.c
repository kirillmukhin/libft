/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/05 15:11:57 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 13:42:35 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function fills the first 'n' bytes of the memory area
** pointed to by 's' with the constant byte 'c'.
** RETURN VALUE
** The ft_memset() function returns a pointer to the memory area 's'.
*/

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*x;
	unsigned char	chr;
	int				i;

	x = s;
	chr = (unsigned char)c;
	i = 0;
	while (n > 0)
	{
		x[i] = chr;
		i++;
		n--;
	}
	return (s);
}
