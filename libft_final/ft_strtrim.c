/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:10:48 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 16:20:41 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a copy of ’s1’
** with the characters specified in ’set’
** removed from the beginning and the end of the string.
** PARAMETERS
** #1. The string to be trimmed.
** #2. The reference set of characters to trim.
** RETURN VALUE
** The trimmed string. NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc
*/

#include <stdio.h>

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	len;
	int		i;

	i = 0;
	if (s1 == NULL)
		return (NULL);
	if (set == NULL)
		return (ft_strdup(s1));
	while (s1[i] != '\0' && ft_strchr(set, s1[i]) != NULL)
		i++;
	len = ft_strlen(&s1[i]);
	while (len != 0 && ft_strchr(set, s1[i + len]))
		len--;
	return (ft_substr(&s1[i], 0, len + 1));
}
