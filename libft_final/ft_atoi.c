/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 17:28:07 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/28 20:30:14 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function converts the initial portion of the string
** pointed to by 'nptr' to int. The behavior is the same as
** strtol(nptr, NULL, 10);
** except that ft_atoi() does not detect errors.
** function returns the converted value.
*/

int	ft_atoi(const char *nptr)
{
	int index;
	int value;
	int result;

	index = 0;
	result = 0;
	value = 1;
	while ((nptr[index] >= 9 && nptr[index] <= 13) || nptr[index] == ' ')
		index++;
	if (nptr[index] == '-')
		value = -1;
	if (nptr[index] == '-' || nptr[index] == '+')
		index++;
	while (ft_isdigit(nptr[index]) != 0)
	{
		result = result * 10 + (nptr[index] - '0');
		index++;
	}
	result = result * value;
	return (result);
}
