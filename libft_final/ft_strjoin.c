/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:10:29 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 14:10:19 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a new string,
** which is the result of the concatenation of ’s1’ and ’s2’.
** PARAMETERS
** #1. The prefix string.
** #2. The suffix string.
** RETURN VALUE
** The new string. NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc
*/

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*res;
	size_t	size1;
	size_t	size2;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	size1 = ft_strlen(s1);
	size2 = ft_strlen(s2);
	res = (char*)malloc(sizeof(char) * (size1 + size2 + 1));
	if (res == NULL)
		return (NULL);
	ft_strlcpy(res, s1, size1 + 1);
	ft_strlcat(res, s2, size1 + size2 + 2);
	return (res);
}
