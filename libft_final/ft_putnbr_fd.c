/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:12:32 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 13:45:07 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Outputs the integer ’n’ to the given file descriptor.
** PARAMETERS
** #1. The integer to output.
** #2. The file descriptor on which to write.
** EXTERNAL FUNCTIONS
** write
*/

void	ft_putnbr_fd(int n, int fd)
{
	long int l;

	l = (long int)n;
	if (l < 0)
	{
		ft_putchar_fd('-', fd);
		l = -l;
	}
	if (l >= 10)
	{
		ft_putnbr_fd((l / 10), fd);
		ft_putnbr_fd((l % 10), fd);
	}
	else
		ft_putchar_fd((l + '0'), fd);
}
