/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:52:05 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 10:18:19 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function compares the first (at most) 'n' bytes
** of the two strings 's1' and 's2'.
** The locale is not taken into account.
** The comparison is done using unsigned characters.
** ft_strncmp() returns an integer indicating the result of the comparison,
** as follows:
**	• 0, if the s1 and s2 are equal;
**	• a negative value if s1 is less than s2;
**	• a positive value if s1 is greater than s2;
** RETURN VALUE
** The ft_strncmp() function returns an integer less than, equal to, or greater
** than zero if 's1' (or the first 'n' bytes thereof) is found, respectively,
** to be less than, to match, or be greater than 's2'.
*/

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	while ((i < n) && (s1[i] != '\0' || s2[i] != '\0'))
	{
		if ((s1[i] != s2[i]))
			return ((unsigned char)s1[i] - (unsigned char)s2[i]);
		i++;
	}
	return (0);
}
