/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:51:53 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 14:32:14 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function locates the first occurrence of the null-terminated string
** little in the string big, where not more than 'len' characters are searched.
** Characters that appear after a ‘\0’ character are not searched.
** RETURN VALUE
** If little is an empty string, big is returned;
** if little occurs nowhere in big, NULL is returned;
** otherwise a pointer to the first character
** of the first occurrence of little is returned.
*/

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	i;
	size_t	g;

	i = 0;
	g = 0;
	if (ft_strlen(little) == 0)
		return ((char*)big);
	while ((big[i] != '\0') && (i < len))
	{
		if (little[g] == big[i])
		{
			while ((big[i + g] == little[g]) && ((i + g) < len))
			{
				if (little[g + 1] == '\0')
					return ((char*)&big[i]);
				g++;
			}
			g = 0;
		}
		i++;
	}
	return (NULL);
}
