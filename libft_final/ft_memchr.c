/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 22:49:30 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 13:27:15 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function scans the initial 'n' bytes of the memory area
** pointed to by 's' for the first instance of 'c'.
** Both 'c' and the bytes of the memory area pointed to by 's'
** are  interpreted as unsigned char.
** RETURN VALUE
** The ft_memchr() function returns a pointer to the matching byte or NULL
** if the character does not occur in the given memory area.
*/

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t				i;
	unsigned char		uc;
	const unsigned char	*us;

	us = (unsigned char*)s;
	uc = (unsigned char)c;
	i = 0;
	while (i < n)
	{
		if (us[i] == uc)
			return ((void*)&us[i]);
		i++;
	}
	return (NULL);
}
