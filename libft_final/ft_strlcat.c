/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:50:30 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 14:16:40 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function appends the NUL-terminated string 'src' to the end of 'dst'.
** It will append at most size - ft_strlen(dst) - 1 bytes,
** NUL-terminating the result.
** RETURN VALUES
** The ft_strlcat() function returns the total length of the string
** it tried to create. That means the initial length of 'dst' plus
** the length of 'src'. While this may seem somewhat confusing,
** it was done to make truncation detection simple.
**
** Note, however, that if ft_strlcat() traverses size characters
** without finding a NUL, the length of the string is considered to be size
** and the destination string will not be NUL-terminated (since there was no
** space for the NUL). This keeps ft_strlcat() from running off the end of a
** string. In practice this should not happen (as it means that either size
** is incorrect or that dst is not a proper “C” string).
** The check exists to prevent potential security problems in incorrect code.
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dst_len;
	char	*p;
	int		append_space;

	dst_len = ft_strlen(dst);
	p = &dst[dst_len];
	append_space = (size - dst_len);
	if (append_space > 0)
		ft_strlcpy(p, src, append_space);
	if (dst_len >= size)
		return (ft_strlen(src) + size);
	return (dst_len + ft_strlen(src));
}
