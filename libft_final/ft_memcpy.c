/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 22:48:58 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 13:33:48 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function copies 'n' bytes from memory area 'src' to memory area 'dest'.
** The memory areas must not overlap.
** RETURN VALUE
** Function returns a pointer to 'dest'.
*/

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char		*d;
	unsigned char		*s;
	size_t				i;

	if (dest == NULL && src == NULL)
		return (NULL);
	d = (unsigned char*)dest;
	s = (unsigned char*)src;
	i = 0;
	if (dest < src)
		while (i < n)
		{
			d[i] = s[i];
			i++;
		}
	else
	{
		while (n)
		{
			n--;
			d[n] = s[n];
		}
	}
	return (dest);
}
