/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:51:16 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 14:03:44 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function returns a pointer to the first occurrence
** of the character 'c' in the string 's'.
** Here "character" means "byte";
** function does not work with wide or multibyte characters.
** RETURN VALUE
** The ft_strchr() function returns a pointer to the matched character
** or NULL if the character is not found. The terminating null byte
** is considered part of the string, so that if 'c' is specified as
** '\0', these functions return a pointer to the terminator.
*/

char	*ft_strchr(const char *s, int c)
{
	int i;

	i = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
			return ((char*)&s[i]);
		i++;
	}
	if (c == '\0')
		return ((char*)&s[i]);
	return (NULL);
}
