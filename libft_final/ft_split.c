/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:11:09 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 13:59:46 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns an array of strings
** obtained by splitting ’s’ using the character ’c’ as a delimiter.
** The array must be ended by a NULL pointer.
** PARAMETERS
** #1. The string to be split.
** #2. The delimiter character.
** RETURN VALUE
** The array of new strings resulting from the split.
** NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc, free
*/

/*
** Clearing array and freeing its memory.
** Subfunction called when memory allocation fails at some point.
*/

static	char	**sf_free_arr(char **arr)
{
	size_t i;

	i = 0;
	while (arr[i])
	{
		if (arr[i] != NULL)
			free(arr[i]);
		i++;
	}
	free(arr);
	return (NULL);
}

/*
** Count the number of strings that will come from splitting 's'
** by its delimeter 'c'.
*/

static	size_t	sf_row_count(char const *s, char c)
{
	size_t	rows;
	int		i;

	rows = 1;
	i = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
		{
			while (s[i] == c)
				i++;
			rows++;
		}
		i++;
	}
	return (rows);
}

/*
** Create, fill and return string with character
*/

static	char	*sf_fill_str(char const *s, char c)
{
	char		*string;
	size_t		i;

	i = 0;
	while (s[i] != c && s[i] != '\0')
		i++;
	string = ft_substr(s, 0, i);
	return (string);
}

/*
** Add strings to the created array of arrays.
**If memory allocation for any string fails — free the entire array.
*/

static char		**sf_add_str(char **arr, char const *s, char c)
{
	int	i;
	int	string;

	i = 0;
	string = 0;
	while (s[i] != '\0')
	{
		if (s[i] != c)
		{
			arr[string] = sf_fill_str(&s[i], c);
			while (s[i] != c && s[i] != '\0')
				i++;
			if (arr[string] == NULL)
			{
				sf_free_arr(arr);
				return (NULL);
			}
			string++;
		}
		else
			i++;
	}
	arr[string] = NULL;
	return (arr);
}

char			**ft_split(char const *s, char c)
{
	char	**arr;
	size_t	rows;

	if (s == NULL)
		return (0);
	rows = sf_row_count(s, c);
	arr = (char**)malloc(sizeof(char*) * (rows + 1));
	if (arr == NULL)
		return (NULL);
	arr = sf_add_str(arr, s, c);
	return (arr);
}
