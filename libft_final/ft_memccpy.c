/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 20:49:04 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 13:21:22 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function copies no more than 'n' bytes
** from memory area 'src' to memory area 'dest',
** stopping when the character 'c' is found.
** If the memory areas overlap, the results are undefined.
** RETURN VALUE
** The ft_memccpy() function returns a pointer
** to the next character in dest after 'c',
** or NULL if 'c' was not found in the first 'n' characters of 'src'.
*/

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char		*d;
	const unsigned char *s;
	size_t				i;

	d = (unsigned char*)dest;
	s = (unsigned char*)src;
	i = 0;
	if (d == NULL && s == NULL)
		return (NULL);
	while (i < n)
	{
		d[i] = s[i];
		if (d[i] == (unsigned char)c)
			return (&d[i + 1]);
		i++;
	}
	return (NULL);
}
