/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 22:49:12 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 10:55:20 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function copies 'n' bytes from memory area 'src' to memory area 'dest'.
** The memory areas may overlap: copying takes place as though
** the bytes in 'src' are first copied into a temporary array
** that does not overlap 'src' or 'dest', and the bytes are then copied
** from the temporary array to 'dest'.
** RETURN VALUE
** The ft_memmove() function returns a pointer to dest.
*/

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	return (ft_memcpy(dest, src, n));
}
