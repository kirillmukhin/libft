/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 23:10:10 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/28 22:40:17 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a substring from the string ’s’.
** The substring begins at index ’start’ and is of maximum size ’len’.
** PARAMETERS
** #1. The string from which to create the substring.
** #2. The start index of the substring in the string ’s’.
** #3. The maximum length of the substring.
** RETURN VALUE
** The substring. NULL if the allocation fails.
** EXTERNAL FUNCTIONS
** malloc
*/

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*res;
	size_t	i;
	size_t	sl;

	if (s == NULL)
		return (NULL);
	sl = ft_strlen(s);
	if (start > sl)
		return (ft_strdup(""));
	if (len > sl)
		len = sl - start;
	res = (char*)malloc(sizeof(char) * (len + 1));
	if (res == NULL)
		return (NULL);
	i = 0;
	while (i < start)
		i++;
	ft_strlcpy(res, &s[i], len + 1);
	return (res);
}
