/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/05 15:13:29 by jarnolf           #+#    #+#             */
/*   Updated: 2020/05/26 01:37:00 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Function calculates the length of the string pointed to by 's',
** excluding the terminating null byte ('\0').
** RETURN VALUE
** The ft_strlen() function returns the number of bytes
** in the string pointed to by 's'.
*/

size_t	ft_strlen(const char *s)
{
	unsigned int i;

	i = 0;
	while (s[i] != '\0')
		i++;
	return (i);
}
