// Separate mains to check libft functions

// FT_ATOI
#include "libft.h"
#include <stdio.h>

int	main()
{
	/*
	"420",
	"-421",
	"0",
	"",
	"  422abc",
	"abc423",
	"++424",
	"--424",
	"♠♠♠123",
	//UNDEFINED BEHAVIOR
	"-9223372036854775828",
	"-9223372036854775028",
	"-9223372036854775812_LONG_MIN-4_undefined_behaviour", //LONG_MIN - 4
	"+9223372036854775811_LONG_MAX+4_undefined_behaviour", //LONG_MAX + 4
	"18446744073709551619_ULONG_MAX+4_undefined_behaviour" //ULONG_MAX + 4
	*/
	char test[] = "00000000099"
	printf("%d\n", ft_atoi(test));
	printf("%d\n", atoi(test));
	return (0);
}

// FT_CALLOC

#include "libft.h"
#include <stdio.h>

int	main()
{
	int size = 32768
	void *t1 = ft_calloc(size, size())
}

// FT_STRNCMP

#include "libft.h"
#include <stdio.h>

int main ()
{
	/*
	ft_strncmp("", "empty", 5)
	//
	ft_strncmp("empty_5", "", 5)
	//
	ft_strncmp("empty_0", "", 0)
	//
	ft_strncmp("same", "same", 4)
	//
	ft_strncmp("almost", "almostsame", 12)
	//
	ft_strncmp("register", "rEgister", 8)
	//
	ft_strncmp("abcdefghij", "abcdefgxyz", 3)
	//
	ft_strncmp("abcdefgh", "abcdwxyz", 4)
	//
	ft_strncmp("zyxbcdefgh", "abcdwxyz", 0)
	//
	ft_strncmp("spam\200", "spam\0", 6)
	*/
	printf("%d\n", ft_strncmp("spam\0", "spam\300", 6));
	printf("%d\n", strncmp("spam\0", "spam\300", 6));
	return(0);
}

// ft_split
#include "libft.h"
#include <stdio.h>

int main()
{
	int i = 0;
	char **arr = ft_split("   split  this string !   ", " ");
	while (i < 5)
	{
		printf("'%s'\n", arr[i]);
		i++;
	}
	return (0);
}


// FT_STRTRIM
#include "libft.h"
#include <stdio.h>

int main()
{
	printf("%s\n", ft_strtrim("qweASDqwe", "eqw"));
	return (0);
}

// FT_SUBSTR

#include "libft.h"
#include <stdio.h>

int main()
{
	/* tests
	// out - empty
	ft_substr("\0",0,1));
	//
	// out - empty
	ft_substr("",0,3));
	//
	// out - 'est'
	ft_substr("test",1,6));
	//
	// out - 'wo'
	ft_substr("onetwo\0threefour\0five",4,14));
	//
	// len > start
	// out - empty
	ft_substr("alla",5,0));
	//
	// start>strlen
	// out empty
	ft_substr("start>strlen",16,16));
	//

	*/
	printf("%s\n", ft_substr("nnnnyyyyn",4,4));
	return(0);
}
